require 'csv'
require 'open-uri'

games = CSV.parse(open("http://nopaystation.com/tsv/PSV_GAMES.tsv"), { :col_sep => "\t", :headers => true })

filtered = games.select {|i| i["PKG direct link"].start_with? "http" and i["zRIF"] != "MISSING" and i["Name"].include?"(3.61+" }.sort_by {|i| i["File Size"].to_i}

result = ""
Dir["PCS*/*.ppk"].each do |game|
	game_title_id = game.split("/")[0]
	game_name = filtered.find {|i| i["Title ID"] == game.split("/")[0] }["Name"]
	result += "#{game}=#{game_name}\n"
end

puts result
